import { createStackNavigator, createSwitchNavigator, createDrawerNavigator, createBottomTabNavigator } from 'react-navigation';

import Splash from '../../modules/splash/splash_container'

const switchNavigator = createSwitchNavigator({
    Splash,
},
    {
        initialRouteName: 'Splash',
        transitionConfig: () => fromLeft(),

    }
)




export default switchNavigator;