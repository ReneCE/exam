import {
    combineReducers
  } from 'redux';
  
  import navigation from './navigation';

  const reducer = combineReducers({
    navigation,
  })
  
  
  export default reducer;