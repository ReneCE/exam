/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';




import {Provider} from 'react-redux';
import {store, persistor} from './src/helpers/utils/store'
import {PersistGate} from 'redux-persist/integration/react';
import AppNavigatorWithState from './src/helpers/navigation/app-navigator-with-state'
import Splash from './src/modules/splash/splash_container'


const App: () => React$Node = () => {
  return (
    <>
      <Provider
        store={store}
      >
        <PersistGate
          loading={<Splash />}
          persistor={persistor}
        >
          <AppNavigatorWithState />
        </PersistGate>
      </Provider>
    </>
  );
};

export default App;